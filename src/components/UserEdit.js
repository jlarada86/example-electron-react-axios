import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { setRedirect, getUser } from "../actions";

 function UserEdit(props) {
    const [user, setUser] = useState(null);
    const [showData, setShow] = useState(false);

    useEffect(() => {
        console.log('props del user edit', props)
        if(props.selectUser.hasOwnProperty('name')){
            setShow(true);
            setUser(props.selectUser);
        }
    }, []);
    return (
        <div>
           { showData &&  <div>
            <h2>{user.name}</h2>
            <p>{user.username}</p>
            <p>{user.phone}</p>
            <p>{user.website}</p>
            <h4>Address</h4>
            <p><span className="badge badge-secondary">Street </span> {user.address.street}</p>
            <p><span className="badge badge-secondary">city </span> {user.address.city}</p>
            <p><span className="badge badge-secondary">suite </span> {user.address.suite}</p>
            <p><span className="badge badge-secondary">zipcode </span> {user.address.zipcode}</p>
            <p><span className="badge badge-secondary">geo </span> { user.address.geo.lat}  {user.address.geo.lng}</p>
            <h4>Company</h4>
            <p><span className="badge badge-secondary">name </span> {user.company.name}</p>
            <p><span className="badge badge-secondary">BS </span> {user.company.bs}</p>
            <p> {user.company.catchPhrase}</p>
               </div>}
            <Link to="/"> Back</Link>
        </div>
    )
}

const mapStateToProps = (state) => ({
    redirect: state.redirect,
    selectUser: state.selectUser
    
});

const mapDispatchToProps = (dispatch) => ({
    setRedirect: (payload) => dispatch(setRedirect(payload)),
    getUser: (payload) => dispatch(getUser(payload))
});

export default connect(mapStateToProps, mapDispatchToProps)(UserEdit);
