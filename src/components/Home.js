import React, { useEffect, useState } from "react";
import Menu from "./Menu/Menu";
import { connect } from "react-redux";
import axios from "axios";
import CardList from "./CardList";
import { setUsersList } from "../actions";
import store from "../store";
import { Redirect } from "react-router-dom";

const Home = (props) => {
  

  useEffect(() => {    

 
    store.subscribe(() => {
        console.log("cambio el store", store.getState().redirect, store.getState().history);
    });

    console.log("store global de componente Home", props);
    
  }, []);

  return (
    <div>
      
      <h1>Test API </h1>
      <CardList users={props.users} history={props.history}/>
    </div>
  );
}

const mapStateToProps = (state) => ({
  ...state,
});

export default connect(mapStateToProps)(Home);
