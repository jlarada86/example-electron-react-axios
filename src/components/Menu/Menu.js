import React, { useEffect, useState } from "react";
import icon from "../../logo.svg";
//import menu from 'path/to/menu';
//import { remote } from 'electron';
import { connect } from "react-redux";
import TitleBar from "frameless-titlebar";
import { setRedirect } from "../../actions";

const electron = window.require("electron");

//const currentWindow = remote.getCurrentWindow();
const currentWindow = electron.remote.getCurrentWindow();

const Menu = (props) => {
  const [redirect, setRedirect] = useState(false);

  return (
    <div>
      <TitleBar
        iconSrc={icon} // app icon
        currentWindow={currentWindow} // electron window instance
        platform={process.platform} // win32, darwin, linux
        menu={[
          
          {
            label: "Desarrollo",
            submenu: [
              {
                label: "Dev tools",
                click() {
                  currentWindow.toggleDevTools();
                },
              },
            ],
          },
        ]}
        theme={
          {
            // any theme overrides specific
            // to your application :)
          }
        }
        title="Example API"
        onClose={() => currentWindow.close()}
        onMinimize={() => currentWindow.minimize()}
        onMaximize={() => currentWindow.maximize()}
        // when the titlebar is double clicked
        onDoubleClick={() => currentWindow.maximize()}
      >
        {/* custom titlebar items */}
      </TitleBar>
    </div>
  );
};

const mapStateToProps = (state) => ({
  ...state,
});

const mapDispatchToProps = (dispatch) => ({
  setRedirect: (payload) => dispatch(setRedirect(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Menu);
