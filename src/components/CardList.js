import React, { useEffect} from 'react'
import CardUser from './Card'
import store from "../store";

export default function CardList(props) {
    

    return (
        <div className="row">
            { props.users.map((user) => { return <CardUser user={user} history={props.history} key={user.name}/> }) }
        </div>
    )
}
