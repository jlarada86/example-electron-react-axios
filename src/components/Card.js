import React, { useEffect } from "react";
import { Card, Button } from "react-bootstrap";
import { connect } from 'react-redux';
import { setRedirect, getUser } from "../actions";

function CardUser(props) {

  const button_callback = () => {
    let r = props.getUser(props.user.name);
    console.log('props now', props, props.user.id);
    props.history.push('/edit/user')
    //console.log('getUser', props.getUser(props.user.id))
  }

    useEffect(() => {
        
    }, []);
    
  return (
    <div className="col-md-3 p-1">
      <Card>
        <Card.Body>
          <Card.Title>{props.user.name}</Card.Title>
          <Card.Text>
            <span>{props.user.username}</span><br/>
            <span>{props.user.phone}</span><br/>
            <span>{props.user.website}</span>
          </Card.Text>
          <Button variant="primary" onClick={button_callback}>See more</Button>
        </Card.Body>
      </Card>
    </div>
  );
}

const mapStateToProps = (state) => ({
    ...state
});

const mapDispatchToProps = (dispatch) => ({
    setRedirect: (payload) => dispatch(setRedirect(payload)),
    getUser: (payload) => dispatch(getUser(payload))
});

export default connect(mapStateToProps, mapDispatchToProps)(CardUser);