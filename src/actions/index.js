import { REDIRECT_APP, SET_USER_LIST, GET_USER } from './constants';

export const setRedirect = (payload) =>{
    return { type: REDIRECT_APP, payload };
}

export const setUsersList = (payload) => {
    return { type: SET_USER_LIST, payload }
}

export const getUser = (payload) => {
    return { type: GET_USER, payload }
}