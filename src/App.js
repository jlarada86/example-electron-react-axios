import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import "bootstrap/dist/css/bootstrap.css";
import { Container } from "react-bootstrap";
import {   
  HashRouter,
  Route,
  Switch,
  withRouter
} from 'react-router-dom';
import Home from './components/Home';
import UserEdit from "./components/UserEdit";
import Menu from "./components/Menu/Menu";
import { connect } from "react-redux";
import { setUsersList } from "./actions";
import axios from "axios";

const {app} = window.require('electron').remote;

class App extends Component {

  componentDidMount(){
    console.log("store global ", this.props)
  }

  componentWillMount(){
    axios
      .get("https://jsonplaceholder.typicode.com/users")
      .then((response) => {
        
        console.log("datos recividos", response.data);
        this.props.setUsersList(response.data);
        
      })
      .catch((error) => {
        
        console.log("Error en la captura de los datos", error);
      });
  }

  render() {
    return (
      <Container className="col-12 c1">    
        <Menu />    
        <HashRouter>
          <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/edit/user" exact component={UserEdit} />
          </Switch>
        </HashRouter>
      </Container>      
    );
  }
}

const mapStateToProps = state => ({
  ...state
});

const mapDispatchToProps = (dispatch) => ({
  setUsersList: (payload) => dispatch(setUsersList(payload)),
});
export default connect(mapStateToProps, mapDispatchToProps)(App);
