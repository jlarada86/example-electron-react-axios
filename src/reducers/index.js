import { REDIRECT_APP, SET_USER_LIST, GET_USER } from "../actions/constants.js";

let initial_state = {
    redirect: "",
    users: [],
    selectUser: {}
};

function rootReducer( state = initial_state, action){

    switch (action.type) {
        case REDIRECT_APP:
            return { ...state, redirect: action.payload }
        case SET_USER_LIST:
            return { ...state, users: action.payload }            
        case GET_USER:
            let user = state.users.filter(user => user.name === action.payload)
            
            return { ...state, selectUser: user[0] };
        default:
            return state;
    }
}

export default rootReducer;