import store from '../store';

store.subscribe( () => {
  console.log("State has changed"  + store.getState());
});

const importElectronIPC = () => {
    const electron = window.require('electron');   
    const ipcRenderer = electron.ipcRenderer;

    return ipcRenderer;
}

const exportFunctions = {
    importElectronIPC  
};

export default exportFunctions;